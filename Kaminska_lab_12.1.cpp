#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
using namespace std;
 float F(float x)
 {
    return exp(fabs(pow(x,2)+x+3));
 }
 
 float L(float y)
 {
   return (log10(fabs(y+pow(y,9))));
 }
 
 int main()
 {
  system("cls");
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
 int i, n;
 float a, aS, aE, ad, b;
  b=3.4*(pow(10,-1));
  aS=3.2;
  aE=5.4;
  ad=0.4; 
  n=(int)((aE-aS)/ad)+1;
  float f[n];
  cout << "Massive F[" << n << "] = ";

  for (a = aS; a <= aE+ad; a=a+ad)
 {
    i++;
    f[i]=(F(a*b)+L(a*b))/(F(b/a)+L(b/a));
    printf(" %7.5f",f[i]);
 }
 
cout << endl;

    system("pause");
 return 0;
}
